---
layout: default
---
## Y... tenía que pasar

Y bueno... Amy y Eric se casan!

Pero eso ya lo sabian, ¿no? Lo que quizás no sabian es que nos encantaría celebrarlo con ustedes muy pronto!

<p class="imgcenter">
<img alt="Amy y Eric" src="assets/images/Amy+Eric.jpg" />
</p>

Ya nos conoces, asíque seamos sinceros: no va a ser sopresa que ninguno de los dos quería algo extravagante, con palomas o mariposas, o con un vestido blanco que se va a usar una sola vez.

Lo que SÍ queremos es compartir este momento con aquellos que más nos importan: USTEDES! Nuestra familia y nuestros amigos más cercanos. Pronto les estaremos enviando una invitación más formal (quizás), pero mientras tanto, considérense invitados desde ya.

Decidimos hacer este sitio porque pensamos que sería más fácil así compartir toda la información que pueden llegar a necesitar. Iremos agregando más contenido a medida que vayamos puliendo los detalles, pero mientras tanto...

### Save the Date!

Estas son las fechas más importantes que se tienen que acordar:

- **22/09/2017** (tentativo): Ceremonia en el Registro Civil  
  Literalmente un trámite de diez minutos en la Oficina del Registro Civil. Nada especial. Firmaremos un par de papeles y saldremos al rato.
  
- **23/09/2017**: Fiestaaaaaaaa  
  Ahora sí, este es el evento principal. El que no se quieren perder, y en el que van a TENER QUE ESTAR. Vengan a celebrar con nosotros mientras disfrutamos de la mejor comida, vinos, y cerveza que Nueva Zelanda tiene para ofrecer.

## Location, location, location...

"¿Cómo?, ¿Nueva Zelanda?"

Y si! todavía no tenemos todos los detalles, pero sabíamos desde el vamos que uno de los dos lados de la famila iba a tener que viajar!

Después de meses de idas y vueltas con las ideas, finalmente decidimos celebrar nuestra union en la ciudad donde nos conocimos: Auckland, New Zealand.

Sabemos que viajar al otro lado del mundo -literalmente- puede resultar complicado, y desde ya que (con mucho pesar) vamos a entender si no pueden venir. Pero sería una enorme alegría si pudiesen!

## Vuelos

Para los que vienen desde Argentina, la diferencia horaria es de +15 horas por lo que van a estar viajando al futuro!. Esto significa que tienen que planificar con cuidado: si salen de Argentina un 18 van a llegar el 20 a Nueva Zelanda. Dos días después! Pero no se preocupen que después recuperan ese día a la vuelta. Se los prometemos.

Como el vuelo es de unas 14hs, probablemente quieran tener algo de tiempo para descansar, ducharse, planchar la ropa, o salir a caminar por el centro de la ciudad. Por eso les recomendamos salir el 18/09 como máximo, para llegar el 20/09. Eso les va a dar tiempo de aclimatarse. Por supuesto que si quieren venir antes, serán más que bienvenidos!

Acá les dejamos unos links para buscar vuelos por esas fechas:

- Saliendo desde Córdoba
  - Volando con [LAN](http://booking.lan.com/es_ar/apps/personas/compra?fecha1_dia=18&fecha1_anomes=2017-09&fecha2_dia=04&fecha2_anomes=2017-10&from_city2=AKL&to_city2=COR&auAvailability=1&ida_vuelta=ida_vuelta&vuelos_origen=C%C3%B3rdoba&from_city1=COR&vuelos_destino=Auckland&to_city1=AKL&flex=1&vuelos_fecha_salida_ddmmaaaa=18/09/2017&vuelos_fecha_regreso_ddmmaaaa=04/10/2017&cabina=Y&nadults=1&nchildren=0&ninfants=0)
  - Volando con [Aerolíneas Argentinas](http://ww1.aerolineas.com.ar/arg/bodies/reservas/procesarParametrosSABRE.asp?idSitio=AR&idIdioma=es&idavuelta=returntravel&idIATAOrigen=COR&idIATAOrigenValue=223&idIATADestino=AKL&idIATADestinoValue=208&calendarioIda=18%2f09%2f2017&calendarioHoras=anytimeFromHost&calendarioVuelta=04%2f10%2f2017&classService=ECONOMY&cantidadAdultos=1&cantidadChicos=0&cantidadBebes=0&claseEnCabina=E&pais=&currency=ARS)

- Saliendo desde Buenos Aires
  - Volando con [LAN](http://booking.lan.com/es_ar/apps/personas/compra?fecha1_dia=18&fecha1_anomes=2017-09&fecha2_dia=04&fecha2_anomes=2017-10&from_city2=AKL&to_city2=BUE&auAvailability=1&ida_vuelta=ida_vuelta&vuelos_origen=Buenos%20Aires&from_city1=BUE&vuelos_destino=Auckland&to_city1=AKL&flex=1&vuelos_fecha_salida_ddmmaaaa=18/09/2017&vuelos_fecha_regreso_ddmmaaaa=04/10/2017&cabina=Y&nadults=1&nchildren=0&ninfants=0)
  - Volando con [Aerolíneas Argentinas](http://ww1.aerolineas.com.ar/arg/bodies/reservas/procesarParametrosSABRE.asp?idSitio=AR&idIdioma=es&idavuelta=returntravel&idIATAOrigen=BUE&idIATAOrigenValue=19&idIATADestino=AKL&idIATADestinoValue=208&calendarioIda=18%2f09%2f2017&calendarioHoras=anytimeFromHost&calendarioVuelta=04%2f10%2f2017&classService=ECONOMY&cantidadAdultos=1&cantidadChicos=0&cantidadBebes=0&claseEnCabina=E&pais=&currency=ARS)

## Alojamiento

La ciudad de Auckland es enorme! toma entre tres y cuatro horas atravezarla completa manejando. No quisiéramos que se pierdan ni que lleguen tarde, por lo que les recomendamos que se hospeden en el centro (CBD, Central Business District). Como se imaginarán, hay muchísimas opciones para todos los presupuestos, así que péguenle un vistazo a Expedia o Booking.com

## Alquiler de Autos

Si estan pensando alquilar un auto, a lo mejor les podemos conseguir descuento. Avisanos y charlamos los detalles.

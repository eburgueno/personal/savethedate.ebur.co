---
layout: default
---
## It had to happen

So... Amy and Eric are getting married!

But you already knew that, didn't you? What you may not know is that we'd love to celebrate with you very soon!

<p class="imgcenter">
<img alt="Amy y Eric" src="assets/images/Amy+Eric.jpg" />
</p>

You know us already, so let's be honest: It won't come as a surprise that neither of us wanted something extravagant, with doves and butterflies, and a white dress that will only be worn once.

What we DO want is to share this moment with those whom we care about the most: YOU. Our family and closest friends. While we will send out a more "formal" invitation (maybe), do consider yourself invited from now on.

We made this website because we thought it would make it easier to share all the information you might need. We'll be adding more content as we finalise the details, but for now...

### Save the Date!

These are the important dates you'll need to remember

- **22/09/2017**: Civil Registry Ceremony  
  Literally a ten-minutes errand at the Civil Registry Office. Nothing special, really. We'll just sign a couple of papers and be out in a jiffy.
  
- **23/09/2017**: Parteeeeeey   
  Now, this is the main event. The one you don't want to miss, and the one you HAVE to attend. Let us celebrate together while enjoying the best food, wine, and beer that New Zealand can offer.

## Location, location, location...

"What do you mean, New Zealand?"

Why yes! while we don't have all the details finalised yet, it was clear from the beginning that one side of the family was going to have to travel!

After months of going back and forth with our ideas, we have decided to celebrate our union in the city where we met: Auckland, New Zealand.

We know travelling to literally the other side of the world might be though, and (with a heavy heart) we will definitely understand if you can't make it. But it would bring us absolute joy if you could!

## Flights

For those of you coming from Argentina, the time difference is +15hs so you will travelling to the future! This means that you'll need to plan carefully: if you depart Argentina on th 18th you will land in New Zealand on the 20th. Two days after you left! Not to worry. You will get your day back upon your return. We promise.

Because the flight takes about 14hs, you will probably want to have some time to rest, get a shower, iron your clothes, and maybe walk around the city. We will thus recommend that you catch your flight on the 18th of September at the latest. That should give you plenty of time to aclimatize. Of course, if you'd like to come earlier you will be most welcome.

Here are some handy links for your convenience:

- Departing from Córdoba
  - With [LAN](http://booking.lan.com/es_ar/apps/personas/compra?fecha1_dia=20&fecha1_anomes=2017-09&fecha2_dia=04&fecha2_anomes=2017-10&from_city2=AKL&to_city2=COR&auAvailability=1&ida_vuelta=ida_vuelta&vuelos_origen=C%C3%B3rdoba&from_city1=COR&vuelos_destino=Auckland&to_city1=AKL&flex=1&vuelos_fecha_salida_ddmmaaaa=20/09/2017&vuelos_fecha_regreso_ddmmaaaa=04/10/2017&cabina=Y&nadults=1&nchildren=0&ninfants=0)
  - With [Aerolíneas Argentinas](http://ww1.aerolineas.com.ar/arg/bodies/reservas/procesarParametrosSABRE.asp?idSitio=AR&idIdioma=es&idavuelta=returntravel&idIATAOrigen=COR&idIATAOrigenValue=223&idIATADestino=AKL&idIATADestinoValue=208&calendarioIda=20%2f09%2f2017&calendarioHoras=anytimeFromHost&calendarioVuelta=04%2f10%2f2017&classService=ECONOMY&cantidadAdultos=1&cantidadChicos=0&cantidadBebes=0&claseEnCabina=E&pais=&currency=ARS)

- Departing from Buenos Aires
  - With [LAN](http://booking.lan.com/es_ar/apps/personas/compra?fecha1_dia=20&fecha1_anomes=2017-09&fecha2_dia=04&fecha2_anomes=2017-10&from_city2=AKL&to_city2=BUE&auAvailability=1&ida_vuelta=ida_vuelta&vuelos_origen=Buenos%20Aires&from_city1=BUE&vuelos_destino=Auckland&to_city1=AKL&flex=1&vuelos_fecha_salida_ddmmaaaa=20/09/2017&vuelos_fecha_regreso_ddmmaaaa=04/10/2017&cabina=Y&nadults=1&nchildren=0&ninfants=0)
  - With [Aerolíneas Argentinas](http://ww1.aerolineas.com.ar/arg/bodies/reservas/procesarParametrosSABRE.asp?idSitio=AR&idIdioma=es&idavuelta=returntravel&idIATAOrigen=BUE&idIATAOrigenValue=19&idIATADestino=AKL&idIATADestinoValue=208&calendarioIda=20%2f09%2f2017&calendarioHoras=anytimeFromHost&calendarioVuelta=04%2f10%2f2017&classService=ECONOMY&cantidadAdultos=1&cantidadChicos=0&cantidadBebes=0&claseEnCabina=E&pais=&currency=ARS)

## Where to Stay

Auckland City is huge! it takes about three to four hours to drive through it end to end. We wouldn't want you to get lost, nor to be late, so we recommend that you stay in the CBD (Central Business District). There is a huge range of accomodation options to suit any budget, so just head over to Expedia or Booking.com

## Car Rentals

If you are planning to rent a car, we *might* be able to get you a discount. Just let us know and we can discuss the details.
